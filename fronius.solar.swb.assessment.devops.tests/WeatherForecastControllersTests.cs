using System.Linq;
using fronius.solar.swb.assessment.devops.Controllers;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace fronius.solar.swb.assessment.devops.tests
{
    public class WeatherForecastControllersTests
    {
        [Fact]
        public void Get_ValidState_ForecastDataReturned()
        {
            //arrange
            WeatherForecastController weatherForecastController = new WeatherForecastController(NullLogger<WeatherForecastController>.Instance);

            //act
            var forecastData = weatherForecastController.Get();

            //assert
            Assert.Equal(5,forecastData.Count());
        }
    }
}