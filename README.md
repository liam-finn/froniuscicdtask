# TASK

You are a DevOps engineer in a team which builds a product for monitoring weather forecasts. The project consists of one .NET 5 project for providing weather forecast data and one .NET 5 test project. Your task is to build up a CI Pipeline including the following stages:

* *build*: build the dotnet project, this stage should fail if build errors occur.
* *test*: run the unit tests of the test project and calculate the test coverage. The pipeline should fail if any tests fail.
* *publish*: build a docker image of the project and publish it to docker hub (docker hub provides free accounts)

The build and test stage should run on any branch, but the puplish stage should only be triggered for the main branch.  
Figure 1 shows a schematic repsentation of the CI Pipeline.

![](ci.png) 
Figure 1: CI Pipeline+

You are free to choose the tools to create this CI pipeline !
=======
