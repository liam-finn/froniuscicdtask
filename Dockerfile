# Base image definition
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
# Copying files to Docker directory
WORKDIR /docker
COPY ["fronius.solar.swb.assessment.devops/fronius.solar.swb.assessment.devops.csproj", "./"]
# Restore
RUN dotnet restore
COPY . ./
# Publish
RUN dotnet publish -c Release -o out
# Runtime
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /docker
EXPOSE 80
COPY --from=build-env /docker/out .
ENTRYPOINT ["dotnet", "fronius.solar.swb.assessment.devops.dll"]
